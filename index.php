<?php 
    require ("animal.php");
    require ("Frog.php");
    require ("Ape.php");

    $sheep = new Animal("shaun");

    echo "<strong>Parent Class (Animal)</strong> <br>";
    echo "Nama : " . $sheep -> getName(); // "shaun"
    echo "<br>";
    echo "Jumlah kaki : " . $sheep -> getLegs(); // 2
    echo "<br>";
    echo "Berdarah dingin : " . $sheep -> getCold_Blooded(); // false

    echo "<br><br>";

    
    $sungokong = new Ape("kera sakti");

    echo "<strong>Child Class (Ape)</strong> <br>";
    echo "Nama : " . $sungokong -> getName();
    echo "<br>";
    echo "Jumlah kaki : " . $sungokong -> getLegs();
    echo "<br>";
    echo "Berdarah dingin : " . $sungokong -> getCold_Blooded();
    echo "<br>";
    echo "Berteriak : " . $sungokong -> yell(); // "Auooo"

    echo "<br><br>";

    
    $kodok = new Frog("buduk");

    echo "<strong>Child Class (Frog)</strong> <br>";
    echo "Nama : " . $kodok -> getName();
    echo "<br>";
    echo "Jumlah kaki : " . $kodok -> getLegs();
    echo "<br>";
    echo "Berdarah dingin : " . $kodok -> getCold_Blooded();
    echo "<br>";
    echo "Melompat : " . $kodok -> jump() ; // "hop hop"

?>