<?php 
    class Frog extends Animal{

        private $legs = 4;

        public function getLegs(){
            return $this -> legs;
        }

        public function jump(){
            return "hop hop";
        }
    }

?>