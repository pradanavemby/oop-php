<?php 
    class Animal{

        private $name;
        private $legs = 2;
        private $cold_blooded = "false";

        public function __construct($name){
            $this -> name = $name;
        }

        public function getName(){
            return $this -> name;
        }

        public function getLegs(){
            return $this -> legs;
        }

        public function getCold_Blooded(){
            return $this -> cold_blooded;
        }
    }

?>